import { defineStore } from 'pinia'
import { getAuth, createUserWithEmailAndPassword, GoogleAuthProvider, onAuthStateChanged, signInWithEmailAndPassword, signInWithPopup, signOut } from 'firebase/auth'
import { collection, doc, getDoc, getDocs, where } from 'firebase/firestore'
import { db } from '@/main'
import { useUsersStore } from '@/stores/users'
import { usePostsStore } from '@/stores/posts'

export const useAuthStore = defineStore({
  id: 'auth',

  state: () => ({
    authId: null,
    authUserUnsubscribe: null,
    authObserverUnsubscribe: null
  }),

  getters: {
    authUser() {
      const users = useUsersStore()
      return users.user(this.authId)
    }
  },

  actions: {
    setAuthId(id) {
      this.authId = id
    },
    setAuthUserUnsubscribe(unsub) {
      this.authUserUnsubscribe = unsub
    },
    setAuthObserverUnsubscribe(unsub) {
      this.authObserverUnsubscribe = unsub
    },
    initAuthentication() {
      if (this.authObserverUnsubscribe) this.authObserverUnsubscribe()
      return new Promise((resolve) => {
        console.log('👣 the user has changed')
        const auth = getAuth()
        const unsub = onAuthStateChanged(auth, async (user) => {
          this.unsubscribeAuthUserSnapshot()
          if (user) {
            await this.fetchAuthUser()
            resolve(user)
          } else {
            resolve(null)
          }
        })
        this.setAuthObserverUnsubscribe(unsub)
      })
    },
    async registerUserWithEmailAndPassword({ avatar = null, email, name, username, password }) {
      const users = useUsersStore()
      const auth = getAuth()
      const result = await createUserWithEmailAndPassword(auth, email, password)
      await users.createUser({ id: result.user.uid, email, name, username, avatar })
    },
    async signInWithEmailAndPassword({ email, password }) {
      const auth = getAuth()
      return await signInWithEmailAndPassword(auth, email, password)
    },
    async signInWithGoogle() {
      const users = useUsersStore()
      const provider = new GoogleAuthProvider()
      const auth = getAuth()
      const { user } = await signInWithPopup(auth, provider)
      const userRef = doc(db, 'users', user.uid)
      const userDoc = await getDoc(userRef)
      if (!userDoc.exists()) {
        const newUser = {
          id: user.uid,
          email: user.email,
          name: user.displayName,
          username: user.email,
          avatar: user.photoURL
        }
        return users.createUser(newUser)
      }
    },
    async signOut() {
      const auth = getAuth()
      signOut(auth)
      this.setAuthId(null)
    },
    async fetchAuthUser() {
      const users = useUsersStore()
      const auth = getAuth()
      const userId = auth.currentUser?.uid
      if (!userId) return
      await users.fetchUser({ id: userId, handleUnsubscribe: (unsub) => {
        this.setAuthUserUnsubscribe(unsub)
      } })
      this.setAuthId(userId)
    },
    async fetchAuthUsersPosts() {
      const postsStore = usePostsStore()
      const posts = await getDocs(collection(db, 'posts'), where('userId', '==', this.authId))
      posts.forEach((item) => {
        postsStore.setPost(item)
      })
    },
    async unsubscribeAuthUserSnapshot() {
      if (this.authUserUnsubscribe) {
        this.authUserUnsubscribe()
        this.setAuthUserUnsubscribe(null)
      }
    },
  }
})
