import { defineStore } from 'pinia'
import { doc, onSnapshot } from 'firebase/firestore'
import { db } from '@/main'
import { docToResource, findById, upsert } from '@/helpers'

export const useForumStore = defineStore({
  id: 'forum',

  state: () => ({
    items: [],
    showPage: false,
    unsubscribes: []
  }),

  getters: {
    getForumsForCategory: (state) => (categoryId) =>
      state.items.filter((f) => f.categoryId === categoryId),
  },

  actions: {
    enableShowPage() {
      this.showPage = true
    },
    disableShowPage() {
      this.showPage = false
    },
    appendUnsubscribe({ unsub }) {
      this.unsubscribes.push(unsub)
    },
    clearAllUnsubscribes() {
      this.unsubscribes = []
    },
    appendThreadToForum({ forumId, threadId }) {
      const forum = findById(this.items, forumId)
      if (!forum) {
        console.warn('Appending to forum failed')
        return
      }
      forum.threads = forum.threads || []
      if (!forum.threads.includes(threadId)){
        forum.threads.push(threadId)
      }
    },
    setItem({ resource, item }) {
      upsert(this.items, docToResource(item))
    },
    fetchForum({ id }) {
      return this.fetchItem({ id, emoji: '🏁', resource: 'forums' })
    },
    fetchForums({ ids }) {
      return this.fetchItems({ ids, emoji: '🏁', resource: 'forums' })
    },
    fetchItem({ id, emoji, resource, handleUnsubscribe = null }) {
      console.log('🔥', emoji, id)
      return new Promise((resolve) => {
        const unsub = onSnapshot(doc(db, resource, id), (doc) => {
          if (doc.exists()) {
            const item = { ...doc.data(), id: doc.id }
            this.setItem({ resource, item })
            resolve(item)
          } else {
            resolve(null)
          }
        })
        if (handleUnsubscribe) {
          handleUnsubscribe(unsub)
        } else {
          this.appendUnsubscribe({ unsub })
        }
      })
    },
    fetchItems({ ids, emoji, resource }) {
      return Promise.all(ids.map(id => this.fetchItem({ id, resource, emoji })))
    },
    async unsubscribeAllSnapshots() {
      this.unsubscribes.forEach(unsub => unsub())
      this.clearAllUnsubscribes()
    },
  },
})
