import { defineStore } from 'pinia'
import { doc, getDoc, onSnapshot, serverTimestamp, setDoc, updateDoc } from 'firebase/firestore'
import { db } from '@/main'
import { docToResource, findById } from '@/helpers'
import { usePostsStore } from '@/stores/posts'
import { useThreadsStore } from '@/stores/threads'
import { upsert } from '@/helpers'

export const useUsersStore = defineStore({
  id: 'users',

  state: () => ({
    items: [],
    unsubscribes: []
  }),

  getters: {
    user: (state) => (id) => {
      const posts = usePostsStore()
      const threads = useThreadsStore()
      const user = findById(state.items, id)
      if (!user) return null
      return {
        ...user,
        get posts() {
          return posts.items.filter((p) => p.userId === user.id)
        },
        get postsCount() {
          return user.postsCount || 0
        },
        get threads() {
          return threads.items.filter((t) => t.userId === user.id)
        },
        get threadsCount() {
          return user.threads?.length || 0
        },
      }
    }
  },

  actions: {
    async createUser({ id, email, name, username, avatar = null }){
      const registeredAt = serverTimestamp()
      const usernameLower = username.toLowerCase()
      email = email.toLowerCase()
      const user = { avatar, email, name, username, usernameLower, registeredAt }
      const userRef = doc(db, 'users', id)
      await setDoc(userRef, user)
      const newUser = await getDoc(userRef)
      this.setUser(newUser)
      return docToResource(newUser)
    },
    async updateUser(user) {
      const updates = {
        avatar: user.avatar || null,
        username: user.username || null,
        name: user.name || null,
        bio: user.bio || null,
        website: user.website || null,
        email: user.email || null,
        location: user.location || null
      }
      const userRef = doc(db, 'users', user.id)
      await updateDoc(userRef, updates)
      this.setUser(user)
    },
    fetchUser({ id, handleUnsubscribe = null }) {
      // const forum = useForumStore()
      // return forum.fetchItem({ id, emoji: '🙋🏻', resource: 'users' })
      console.log('🔥', '🙋🏻', id)
      return new Promise((resolve) => {
        const unsub = onSnapshot(doc(db, 'users', id), (doc) => {
          if (doc.exists()) {
            const user = { ...doc.data(), id: doc.id }
            this.setUser(user)
            resolve(user)
          } else {
            resolve(null)
          }
        })
        if (handleUnsubscribe) {
          handleUnsubscribe(unsub)
        } else {
          this.appendUnsubscribe({ unsub })
        }
      })
    },
    fetchUsers({ ids }) {
      // const forum = useForumStore()
      // return forum.fetchItems({ ids, emoji: '🙋🏻', resource: 'users' })
      return Promise.all(ids.map(id => this.fetchUser({ id })))
    },
    setUser(user) {
      upsert(this.items, docToResource(user))
    },
    appendThreadToUser({ userId, threadId }) {
      const user = findById(this.items, userId)
      if (!user) {
        console.warn('Appending to user failed')
        return
      }
      user.threads = user.threads || []
      if (!user.threads.includes(threadId)){
        user.threads.push(threadId)
      }
    },
    appendUnsubscribe({ unsub }) {
      this.unsubscribes.push(unsub)
    },
    clearAllUnsubscribes() {
      this.unsubscribes = []
    },
  }
})
