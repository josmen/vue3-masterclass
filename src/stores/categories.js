import { defineStore } from 'pinia'
import { collection, doc, onSnapshot } from 'firebase/firestore'
import { db } from '@/main'
import { docToResource, upsert } from '@/helpers'

export const useCategoriesStore = defineStore({
  id: 'categories',

  state: () => ({
    items: [],
    unsubscribes: []
  }),

  getters: {},

  actions: {
    fetchCategory({ id, handleUnsubscribe = null }) {
      // const forum = useForumStore()
      // return forum.fetchItem({ id, emoji: '🏷', resource: 'categories' })
      console.log('🔥', '🏷', id)
      return new Promise((resolve) => {
        const unsub = onSnapshot(doc(db, 'categories', id), (doc) => {
          if (doc.exists()) {
            const item = { ...doc.data(), id: doc.id }
            this.setCategory(item)
            resolve(item)
          } else {
            resolve(null)
          }
        })
        if (handleUnsubscribe) {
          handleUnsubscribe(unsub)
        } else {
          this.appendUnsubscribe({ unsub })
        }
      })
    },
    fetchCategories({ ids }) {
      // const forum = useForumStore()
      // return forum.fetchItems({ ids, emoji: '🏷', resource: 'categories' })
      return Promise.all(ids.map(id => this.fetchCategory({ id })))
    },
    fetchAllCategories() {
      console.log('🔥', '🏷', 'all')
      return new Promise((resolve) => {
        onSnapshot(collection(db, 'categories'), (querySnapshot) => {
          const categories = []
          querySnapshot.forEach(doc => {
            const item = { id: doc.id, ...doc.data() }
            categories.push(item)
            this.setCategory(item)
          })
          resolve(categories)
        })
      })
    },
    setCategory(category) {
      upsert(this.items, docToResource(category))
    },
    appendUnsubscribe({ unsub }) {
      this.unsubscribes.push(unsub)
    },
    clearAllUnsubscribes() {
      this.unsubscribes = []
    },
  }
})
