import { defineStore } from 'pinia'
import { arrayUnion, collection, doc, getDoc, increment, onSnapshot, serverTimestamp, updateDoc, writeBatch } from 'firebase/firestore'
import { db } from '@/main'
import { useAuthStore } from '@/stores/auth'
import { useThreadsStore } from '@/stores/threads'
import { docToResource, upsert } from '@/helpers'

export const usePostsStore = defineStore({
  id: 'posts',

  state: () => ({
    items: [],
    unsubscribes: []
  }),

  getters: {},

  actions: {
    async createPost(post) {
      const auth = useAuthStore()
      const threads = useThreadsStore()
      post.userId = auth.authId
      post.publishedAt = serverTimestamp()
      const postRef = doc(collection(db, 'posts'))
      const threadRef = doc(db, 'threads', post.threadId)
      const userRef = doc(db, 'users', auth.authId)
      const batch = writeBatch(db)
      batch.set(postRef, post)
      batch.update(threadRef, {
        posts: arrayUnion(postRef.id),
        contributors: arrayUnion(this.authId)
      })
      batch.update(userRef, {
        postsCount: increment(1)
      })
      await batch.commit()
      const newPost = await getDoc(postRef)
      this.setPost({ ...newPost.data(), id: newPost.id })
      // forum.setItem({ resource: 'posts', item: { ...newPost.data(), id: newPost.id } })
      threads.appendPostToThread({ threadId: post.threadId, postId: newPost.id })
      threads.appendContributorToThread({ threadId: post.threadId, authId: post.userId })
    },
    async updatePost({ text, id }) {
      const auth = useAuthStore()
      const post = {
        text,
        edited: {
          at: serverTimestamp(),
          by: auth.authId,
          moderated: false
        }
      }
      const postRef = doc(db, 'posts', id)
      await updateDoc(postRef, post)
      const updatedPost = await getDoc(postRef)
      this.setPost(updatedPost)
    },
    fetchPost({ id, handleUnsubscribe = null }) {
      // const forum = useForumStore()
      // return forum.fetchItem({ id, emoji: '💬', resource: 'posts' })
      console.log('🔥', '💬', id)
      return new Promise((resolve) => {
        const unsub = onSnapshot(doc(db, 'posts', id), (doc) => {
          if (doc.exists()) {
            const item = { ...doc.data(), id: doc.id }
            this.setPost(item)
            resolve(item)
          } else {
            resolve(null)
          }
        })
        if (handleUnsubscribe) {
          handleUnsubscribe(unsub)
        } else {
          this.appendUnsubscribe({ unsub })
        }
      })
    },
    fetchPosts({ ids }) {
      // const forum = useForumStore()
      // return forum.fetchItems({ ids, emoji: '💬', resource: 'posts' })
      return Promise.all(ids.map(id => this.fetchPost({ id })))
    },
    setPost(post) {
      upsert(this.items, docToResource(post))
    },
    appendUnsubscribe({ unsub }) {
      this.unsubscribes.push(unsub)
    },
    clearAllUnsubscribes() {
      this.unsubscribes = []
    },
  }
})
