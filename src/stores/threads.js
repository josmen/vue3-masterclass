import { defineStore } from 'pinia'
import { arrayUnion, collection, doc, getDoc, onSnapshot, serverTimestamp, writeBatch } from 'firebase/firestore'
import { db } from '@/main'
import { docToResource, findById, upsert } from '@/helpers'
import { useUsersStore } from '@/stores/users'
import { useAuthStore } from '@/stores/auth'
import { useForumStore } from '@/stores/forum'
import { usePostsStore } from '@/stores/posts'

export const useThreadsStore = defineStore({
  id: 'threads',

  state: () => ({
    items: [],
    unsubscribes: []
  }),

  getters: {
    thread: (state) => (id) => {
      const users = useUsersStore()
      const thread = findById(state.items, id)
      if (!thread) return {}
      return {
        ...thread,
        get author() {
          return findById(users.items, thread.userId)
        },
        get repliesCount() {
          return thread.posts.length - 1
        },
        get contributorsCount() {
          return thread.contributors ? thread.contributors.length : 0
        }
      }
    }
  },

  actions: {
    async createThread({ title, text, forumId }) {
      const auth = useAuthStore()
      const forum = useForumStore()
      const users = useUsersStore()
      const posts = usePostsStore()
      const userId = auth.authId
      const publishedAt = serverTimestamp()
      const threadRef = doc(collection(db, 'threads'))
      const thread = { forumId, title, publishedAt, userId, id: threadRef.id }
      const userRef = doc(db, 'users', userId)
      const forumRef = doc(db, 'forums', forumId)
      const batch = writeBatch(db)
      batch.set(threadRef, thread)
      batch.update(userRef, {
        threads: arrayUnion(threadRef.id)
      })
      batch.update(forumRef, {
        threads: arrayUnion(threadRef.id)
      })
      await batch.commit()
      const newThread = await getDoc(threadRef)
      this.setThread({ ...newThread.data(), id: newThread.id })
      users.appendThreadToUser({ userId, threadId: threadRef.id })
      forum.appendThreadToForum({ forumId, threadId: threadRef.id })
      await posts.createPost({ text, threadId: threadRef.id })
      return findById(this.items, threadRef.id)
    },
    async updateThread({ title, text, threadId }) {
      const posts = usePostsStore()
      const thread = findById(this.items, threadId)
      const post = findById(posts.items, thread.posts[0])
      let newThread = { ...thread, title }
      let newPost = { ...post, text }
      const threadRef = doc(db, 'threads', threadId)
      const postRef = doc(db, 'posts', post.id)
      const batch = writeBatch(db)
      batch.update(threadRef, newThread)
      batch.update(postRef, newPost)
      await batch.commit()
      newThread = await getDoc(threadRef)
      newPost = await getDoc(postRef)
      this.setThread(newThread)
      posts.setPost(newPost)
      return docToResource(newThread)
    },
    fetchThread({ id, handleUnsubscribe = null }) {
      // const forum = useForumStore()
      // return forum.fetchItem({ id, emoji: '📄', resource: 'threads' })
      console.log('🔥', '📄', id)
      return new Promise((resolve) => {
        const unsub = onSnapshot(doc(db, 'threads', id), (doc) => {
          if (doc.exists()) {
            const item = { ...doc.data(), id: doc.id }
            this.setThread(item)
            resolve(item)
          } else {
            resolve(null)
          }
        })
        if (handleUnsubscribe) {
          handleUnsubscribe(unsub)
        } else {
          this.appendUnsubscribe({ unsub })
        }
      })
    },
    fetchThreads({ ids }) {
      // const forum = useForumStore()
      // return forum.fetchItems({ ids, emoji: '📄', resource: 'threads' })
      return Promise.all(ids.map(id => this.fetchThread({ id })))
    },
    setThread(thread) {
      upsert(this.items, docToResource(thread))
    },
    appendPostToThread({ threadId, postId }) {
      const thread = findById(this.items, threadId)
      if (!thread) {
        console.warn('Appending to thread failed')
        return
      }
      thread.posts = thread.posts || []
      if (!thread.posts.includes(postId)){
        thread.posts.push(postId)
      }
    },
    appendContributorToThread({ threadId, authId }) {
      const thread = findById(this.items, threadId)
      if (!thread) {
        console.warn('Appending to thread failed')
        return
      }
      thread.contributors = thread.contributors || []
      if (!thread.contributors.includes(authId)){
        thread.contributors.push(authId)
      }
    },
    appendUnsubscribe({ unsub }) {
      this.unsubscribes.push(unsub)
    },
    clearAllUnsubscribes() {
      this.unsubscribes = []
    },
  }
})
