import { ref } from 'vue'
import { useForumStore } from '../stores/forum'

export function useAsyncDataStatus() {
  const { enableShowPage } = useForumStore()

  const ready = ref(false)

  const fetched = () => {
    ready.value = true
    enableShowPage()
  }

  return {
    ready,
    fetched,
  }
}
