import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import firebaseConfig from '@/config/firebase'
import router from '@/router'
import App from '@/App.vue'
import AppDate from '@/components/AppDate.vue'
import AppSpinner from '@/components/AppSpinner.vue'
import FontAwesome from '@/plugins/FontAwesome'
import ClickOutsideDirective from '@/plugins/ClickOutsideDirective'
import PageScrollDirective from '@/plugins/PageScrollDirective'

const fbApp = initializeApp(firebaseConfig)
const db = getFirestore(fbApp)

const app = createApp(App)
app.use(createPinia())
app.use(router)
app.use(FontAwesome)
app.use(ClickOutsideDirective)
app.use(PageScrollDirective)
app.component('AppDate', AppDate)
app.component('AppSpinner', AppSpinner)

router.isReady().then(() => app.mount('#app'))

export { db }
